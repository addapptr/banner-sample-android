# Banner ads sample for Gravite

This app demonstrates banner ads integration using AATKit. It presents how the banner ads can be integrated within RecyclerView having multiple rows of content.
See also the [wiki](https://aatkit.gitbook.io/android-integration/formats/banner) for more instrucions.

---
## Installation
Clone this repository and import into **Android Studio**
```bash
git clone git@bitbucket.org:addapptr/banner-sample-android.git
```
---
## Maintainers
Current maintainers:

* [Damian Supera](https://bitbucket.org/damian_s/)
* [Michał Kapuściński](https://bitbucket.org/m_kapuscinski/)